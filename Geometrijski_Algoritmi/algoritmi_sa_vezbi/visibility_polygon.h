#ifndef VISIBILITY_POLYGON_H
#define VISIBILITY_POLYGON_H

#include "algoritambaza.h"

class visibility_polygon : public AlgoritamBaza
{
public:
    visibility_polygon(QWidget *pCrtanje,
                       int pauzaKoraka,
                       const bool &naivni = false,
                       std::string imeDatoteke = "",
                       int brojDuzi = BROJ_SLUCAJNIH_OBJEKATA);

    void pokreniAlgoritam() final;
    void crtajAlgoritam(QPainter *painter) const final;
    void pokreniNaivniAlgoritam() final;
    void crtajNaivniAlgoritam(QPainter *painter) const final;

private:
    std::vector<QPoint> _tacke;
    unsigned int _n;
};

#endif // VISIBILITY_POLYGON_H
