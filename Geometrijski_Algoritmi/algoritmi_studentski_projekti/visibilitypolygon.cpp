#include "visibilitypolygon.h"
#include <fstream>
#include <set>
#include <QPainterPath>
#include <iostream>

VisibilityPolygon::VisibilityPolygon(QWidget *pCrtanje,
                                     int pauzaKoraka,
                                     const bool &naivni,
                                     std::string imeDatoteke,
                                     int brojTacaka)
    : AlgoritamBaza(pCrtanje, pauzaKoraka, naivni)
{
    if (imeDatoteke == "")
        _tacke = ucitajNasumicneTacke(brojTacaka);
    else
        _tacke = ucitajPodatkeIzDatoteke(imeDatoteke);

    _position = initPosition();
    _edges = initEdges();
}

std::vector<QPointF> VisibilityPolygon::ucitajPodatkeIzDatoteke(std::string imeDatoteke) const
{
    std::ifstream inputFile(imeDatoteke);
    std::vector<QPointF> points;
    int x, y;
    while(inputFile >> x >> y)
        points.emplace_back(x, y);
    return points;
}

std::vector<QPointF> VisibilityPolygon::generisiNasumicneTacke(int brojTacaka) const
{
    std::vector<QPointF> randomPoints;
    std::vector<QPoint> randomPointsInt = AlgoritamBaza::generisiNasumicneTacke(brojTacaka);

    for(int i=0; i < brojTacaka; i++)
        randomPoints.emplace_back(randomPointsInt[i]);

    return randomPoints;
}

std::vector<QPointF> VisibilityPolygon::ucitajNasumicneTacke(int brojTacaka) const
{
    std::vector<QPointF> randomPoints = generisiNasumicneTacke(brojTacaka);
    pomocneFunkcije::sortirajTackeZaProstPoligon(randomPoints);
    return randomPoints;
}

QPointF VisibilityPolygon::initPosition() {
    QPolygonF polygon = QPolygonF(QVector<QPointF>::fromStdVector(_tacke));
    QPointF point;
    while (true) {
        QRectF rect = polygon.boundingRect();
        double x = (double)std::rand() / RAND_MAX;
        x = rect.left() + x * (rect.right() - rect.left());
        double y = (double)std::rand() / RAND_MAX;
        y = rect.bottom() + y * (rect.top() - rect.bottom());
        point = QPointF(x, y);
        if (polygon.containsPoint(point, Qt::OddEvenFill)){
            break;
        }
    }
    return point;
}

void VisibilityPolygon::initPositionFixed(double x, double y) {
    _position = QPointF(x, y);
}

std::vector<QLineF> VisibilityPolygon::initEdges() {
    std::vector<QLineF> edges(_tacke.size());
    for (int i=0; i<_tacke.size(); i++){
        edges[i] = QLineF(_tacke[i], _tacke[(i+1)%_tacke.size()]);
    }
    return edges;
}

std::set<int, std::function<bool (int, int)>> VisibilityPolygon::initSet() {
    auto cmp = [&](int a, int b) {
      return lessThan(a, b);
    };
    std::set<int, std::function<bool (int, int)>> s(cmp);
    return s;
}

QPolygonF VisibilityPolygon::polygon() {
    return QPolygonF(QVector<QPointF>::fromStdVector(_polygon));
}

double VisibilityPolygon::angle(QPointF p1, QPointF p2){
    double result = atan2(p2.y()-p1.y(), p2.x()-p1.x())* 180 / pi;
    return floor(result*1000)/1000;
}

double VisibilityPolygon::angle2(QPointF p1, QPointF p2, QPointF p3){
    double a1 = angle(p1, p2);
    double a2 = angle(p2, p3);
    double a3 = a1 - a2;
    if (a3 < 0)
        a3 += 360;
    if (a3 > 360)
        a3 -= 360;
    return a3;
}

QPointF VisibilityPolygon::intersect(QPointF a1, QPointF a2, QPointF b1, QPointF b2){
    double dbx = b2.x() - b1.x();
    double dby = b2.y() - b1.y();
    double dax = a2.x() - a1.x();
    double day = a2.y() - a1.y();

    double uB = dby*dax - dbx*day;
    if (uB != 0) {
        double ua = (dbx*(a1.y()-b1.y()) - dby*(a1.x()-b1.x())) / uB;
        return QPointF(a1.x()-ua*-dax, a1.y()-ua*-day);
    }
    return QPointF(std::numeric_limits<double>::infinity(), 0);
}

double VisibilityPolygon::distance(QPointF a, QPointF b){
    double dx = a.x() - b.x();
    double dy = a.y() - b.y();
    return sqrt(dx*dx + dy*dy);
}

bool VisibilityPolygon::equals(QPointF a, QPointF b){
    if (abs(a.x()-b.x()) < epsilon && abs(a.y()-b.y()) < epsilon)
        return true;
    return false;
}

bool VisibilityPolygon::lessThan(int i, int j){
    QPointF inter1 = intersect(_edges[i].p1(), _edges[i].p2(), _position, _destination);
    QPointF inter2 = intersect(_edges[j].p1(), _edges[j].p2(), _position, _destination);
    if (!equals(inter1, inter2)) {
        double d1 = distance(inter1, _position);
        double d2 = distance(inter2, _position);
        return d1 < d2;
    }
    bool end1 = false;
    if (equals(inter1, _edges[i].p1()))
        end1 = true;

    bool end2 = false;
    if (equals(inter2, _edges[j].p1()))
        end2 = true;

    double a1, a2;
    if (end1)
        a1 = angle2(_edges[i].p2(), inter1, _position);
    else
        a1 = angle2(_edges[i].p1(), inter1, _position);

    if (end2)
        a2 = angle2(_edges[j].p2(), inter2, _position);
    else
        a2 = angle2(_edges[j].p1(), inter2, _position);

    if (a1 < 180) {
        if (a2 > 180)
            return true;
        return a2 < a1;
    }
    return a1 < a2;
}

std::vector<SortedEdges> VisibilityPolygon::sortEdges(){
    std::vector<SortedEdges> s;
    for (int i=0; i<_edges.size(); i++){
        s.push_back(SortedEdges{i, false, angle(_edges[i].p1(), _position)});
        s.push_back(SortedEdges{i, true, angle(_edges[i].p2(), _position)});
    }
    sort(s.begin(), s.end(), [](const SortedEdges& a, const SortedEdges& b) {
        if (a.angle == b.angle)
            return a.index < b.index;
        return a.angle < b.angle;
    });
    return s;
}

void VisibilityPolygon::pokreniAlgoritam()
{
    std::vector<SortedEdges> sortedEdges = sortEdges();
    _destination = QPointF(_position.x() + 1, _position.y());
    _s = initSet();

    for (int i=0; i<_edges.size(); i++) {
        double a1 = angle(_edges[i].p1(), _position);
        double a2 = angle(_edges[i].p2(), _position);

        bool active = false;
        if (a1 > -180 && a1 <= 0 && a2 <= 180 && a2 >= 0 && a2-a1 > 180)
            active = true;

        if (a2 > -180 && a2 <= 0 && a1 <= 180 && a1 >= 0 && a1-a2 > 180)
            active = true;

        QPointF inter = intersect(_edges[i].p1(), _edges[i].p2(), _position, _destination);
        if (active && inter.x() != std::numeric_limits<double>::infinity())
            _s.insert(i);
    }
    AlgoritamBaza_updateCanvasAndBlock()
    int i = 0;
    while (i < sortedEdges.size()) {

        bool extend = false;
        bool shorten = false;
        int orig = i;
        QPointF vertex;

        if (sortedEdges[i].isFirst)
            vertex = _edges[sortedEdges[i].index].p2();
        else
            vertex = _edges[sortedEdges[i].index].p1();

        int oldSegment = *_s.begin();

        while (sortedEdges[i].angle < sortedEdges[orig].angle+epsilon) {
            auto search = _s.find(sortedEdges[i].index);
            if (search != _s.end()) {
                if (sortedEdges[i].index == oldSegment) {
                    extend = true;

                    if (sortedEdges[i].isFirst)
                        vertex = _edges[sortedEdges[i].index].p2();
                    else
                        vertex = _edges[sortedEdges[i].index].p1();
                }
                _destination = vertex;
                _s.erase(sortedEdges[i].index);
            } else {
                _destination = vertex;
                QPointF inter = intersect(
                        _edges[sortedEdges[i].index].p1(),
                        _edges[sortedEdges[i].index].p2(),
                        _position, _destination);
                if (inter.x() != std::numeric_limits<double>::infinity())
                    _s.insert(sortedEdges[i].index);
                if (*_s.begin() != oldSegment)
                    shorten = true;
            }
            AlgoritamBaza_updateCanvasAndBlock()
            i++;
            if (i == sortedEdges.size())
                break;
        }

        if (extend) {
            _polygon.push_back(vertex);
            QPointF cur = intersect(_edges[*_s.begin()].p1(), _edges[*_s.begin()].p2(), _position, vertex);
            if (!equals(cur, vertex)) {
                _polygon.push_back(cur);
            }
        } else if (shorten) {
            _polygon.push_back(intersect(_edges[oldSegment].p1(), _edges[oldSegment].p2(), _position, vertex));
            _polygon.push_back(intersect(_edges[*_s.begin()].p1(), _edges[*_s.begin()].p2(), _position, vertex));
        }
    }
    _s.clear();
    AlgoritamBaza_updateCanvasAndBlock()
    emit animacijaZavrsila();
}

void VisibilityPolygon::crtajAlgoritam(QPainter *painter) const
{
    if (!painter) return;

    QPen blue = painter->pen();
    blue.setColor(Qt::blue);
    blue.setWidth(3);

    QPen yellow = painter->pen();
    yellow.setColor(Qt::yellow);
    yellow.setWidth(10);

    QPen red = painter->pen();
    red.setColor(Qt::red);
    red.setWidth(5);

    painter->setPen(blue);
    painter->drawPolygon(_tacke.data(), _tacke.size());

    QPolygonF polygon = QPolygonF(QVector<QPointF>::fromStdVector(_polygon));
    QBrush brush = painter->brush();
    brush.setColor(Qt::gray);
    brush.setStyle(Qt::Dense3Pattern);
    QPainterPath path;
    path.addPolygon(polygon);
    painter->fillPath(path, brush);

    painter->setPen(yellow);
    painter->drawPoint(_position);
    if (!_s.empty()){
        painter->drawLine(_position, _destination);
        painter->setPen(red );
        for (auto i : _s)
            painter->drawLine(_edges[i]);
    }
}

bool sideOfLine(QPointF a, QPointF b, QPointF c){
    return ((b.x() - a.x())*(c.y() - a.y()) - (b.y() - a.y())*(c.x() - a.x())) > 0;
}

bool onLine(QPointF a, QPointF b, QPointF c){
    return abs((b.x() - a.x())*(c.y() - a.y()) - (b.y() - a.y())*(c.x() - a.x())) < 0.001;
}

void VisibilityPolygon::pokreniNaivniAlgoritam()
{
    int n = _tacke.size();
    for (int i=0; i<n; i++) {
        QPointF closestPoint;
        double d = std::numeric_limits<double>::max();

        for (int j=0; j<n; j++){
            QLineF e = QLineF(_tacke[j], _tacke[(j+1)%n]);
            QPointF inter;
            QLineF::IntersectionType type = e.intersects(QLineF(_position, _tacke[i]), &inter);

            if (type == QLineF::IntersectionType::BoundedIntersection && distance(_position, inter) < d){
                d = distance(_position, inter);
                closestPoint = inter;
            }
        }

        if (_tacke[i] == closestPoint){
            bool later = false;

            if (i != 0 && onLine(_tacke[i-1], _tacke[i], _polygon[_polygon.size()-1]))
                _polygon.push_back(closestPoint);
            else if (i != 0 && !onLine(_tacke[i-1], _tacke[i], _polygon[_polygon.size()-1]))
                later = true;
            else
                _polygon.push_back(closestPoint);

            if (sideOfLine(_position, _tacke[i], _tacke[(i+1)%n]) == sideOfLine(_position, _tacke[i], _tacke[(i-1)%n])){
                d = std::numeric_limits<double>::max();
                QPointF closestPoint1;
                for (int j=0; j<n; j++){
                    if (i == j || i == (j+1)%n)
                        continue;
                    QLineF e = QLineF(_tacke[j], _tacke[(j+1)%n]);
                    QPointF inter;
                    QLineF::IntersectionType type = e.intersects(QLineF(_position, 100*(_tacke[i]-_position)), &inter);

                    if (type == QLineF::IntersectionType::BoundedIntersection && distance(_position, inter) < d){
                        d = distance(_position, inter);
                        closestPoint1 = inter;
                    }
                }
                if (d != std::numeric_limits<double>::max())
                    _polygon.push_back(closestPoint1);
            }
            if (later) {
                _polygon.push_back(closestPoint);
            }
        }
    }


    emit animacijaZavrsila();
}

void VisibilityPolygon::crtajNaivniAlgoritam(QPainter *painter) const
{
    if (!painter) return;

    QPen blue = painter->pen();
    blue.setColor(Qt::blue);
    blue.setWidth(3);

    QPen yellow = painter->pen();
    yellow.setColor(Qt::yellow);
    yellow.setWidth(10);

    painter->setPen(blue);
    painter->drawPolygon(_tacke.data(), _tacke.size());

    QPolygonF polygon = QPolygonF(QVector<QPointF>::fromStdVector(_polygon));
    QBrush brush = painter->brush();
    brush.setColor(Qt::gray);
    brush.setStyle(Qt::Dense3Pattern);
    QPainterPath path;
    path.addPolygon(polygon);
    painter->fillPath(path, brush);

    painter->setPen(yellow);
    painter->drawPoint(_position);
}

