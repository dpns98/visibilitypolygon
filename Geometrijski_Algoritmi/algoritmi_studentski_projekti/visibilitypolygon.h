#ifndef VISIBILITYPOLYGON_H
#define VISIBILITYPOLYGON_H

#include "algoritambaza.h"
#include "pomocnefunkcije.h"
#include <set>

#define pi 3.14159265
#define epsilon 0.0000001

struct SortedEdges {
    int index;
    bool isFirst;
    double angle;
};

class VisibilityPolygon : public AlgoritamBaza
{
public:
    VisibilityPolygon(QWidget *pCrtanje,
                      int pauzaKoraka,
                      const bool &naivni = false,
                      std::string imeDatoteke = "",
                      int brojTacaka = BROJ_SLUCAJNIH_OBJEKATA);

      void pokreniAlgoritam() final;
      void crtajAlgoritam(QPainter *painter) const final;
      void pokreniNaivniAlgoritam() final;
      void crtajNaivniAlgoritam(QPainter *painter) const final;

      QPolygonF polygon();
      void initPositionFixed(double, double);

  private:
      std::vector<QPointF> ucitajPodatkeIzDatoteke(std::string imeDatoteke) const;
      std::vector<QPointF> generisiNasumicneTacke(int brojTacaka = BROJ_SLUCAJNIH_OBJEKATA) const;
      std::vector<QPointF> ucitajNasumicneTacke(int brojTacaka = BROJ_SLUCAJNIH_OBJEKATA) const;
      std::vector<QPointF> _tacke;

      //arkus tangesa duzi
      double angle(QPointF p1, QPointF p2);
      //ugao izmedju 3 tacke
      double angle2(QPointF p1, QPointF p2, QPointF p3);
      //presek 2 prave
      QPointF intersect(QPointF a1, QPointF a2, QPointF b1, QPointF b2);
      // rastojaje 2 tacke
      double distance(QPointF a, QPointF b);
      //jednakost tacaka
      bool equals(QPointF a, QPointF b);
      //poredjenje ivica poligona u setu
      bool lessThan(int i, int j);
      //sortiranje ivica u odnosu na polaran ugao
      std::vector<SortedEdges> sortEdges();
      QPointF initPosition();
      std::vector<QLineF> initEdges();
      std::set<int, std::function<bool (int, int)>> initSet();

      QPointF _position;
      QPointF _destination;
      std::vector<QLineF> _edges;
      std::vector<QPointF> _polygon;
      std::set<int, std::function<bool (int, int)>> _s;
};

#endif // VISIBILITYPOLYGON_H
