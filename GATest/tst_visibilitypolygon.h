#ifndef TST_VISIBILITYPOLYGON_H
#define TST_VISIBILITYPOLYGON_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "./visibilitypolygon.h"

using namespace testing;

static constexpr auto inputFile1 = "../Geometrijski_Algoritmi/input_files/visibilitypoligon/input1.txt";

TEST(visibility_polygon, testPositionInsidePolygon)
{
    VisibilityPolygon vp(nullptr, 0, false, inputFile1, 20);

    vp.initPositionFixed(122.271, 238.545);
    vp.pokreniAlgoritam();
    auto p = vp.polygon();
    bool cond = p.containsPoint(QPointF(122.271, 238.545), Qt::OddEvenFill);

    EXPECT_TRUE(cond);
}

TEST(visibility_polygon, testPointInsidePolygon1)
{
    VisibilityPolygon vp(nullptr, 0, false, inputFile1, 20);

    vp.initPositionFixed(122.271, 238.545);
    vp.pokreniAlgoritam();
    auto p = vp.polygon();
    bool cond = p.containsPoint(QPointF(200, 300), Qt::OddEvenFill);

    EXPECT_TRUE(cond);
}

TEST(visibility_polygon, testPointInsidePolygon2)
{
    VisibilityPolygon vp(nullptr, 0, false, inputFile1, 20);

    vp.initPositionFixed(122.271, 238.545);
    vp.pokreniAlgoritam();
    auto p = vp.polygon();
    bool cond = p.containsPoint(QPointF(170, 400), Qt::OddEvenFill);

    EXPECT_TRUE(cond);
}

TEST(visibility_polygon, testPointInsidePolygon3)
{
    VisibilityPolygon vp(nullptr, 0, false, inputFile1, 20);

    vp.initPositionFixed(122.271, 238.545);
    vp.pokreniAlgoritam();
    auto p = vp.polygon();
    bool cond = p.containsPoint(QPointF(221, 221), Qt::OddEvenFill);

    EXPECT_TRUE(cond);
}

TEST(visibility_polygon, testPointInsidePolygon4)
{
    VisibilityPolygon vp(nullptr, 0, false, inputFile1, 20);

    vp.initPositionFixed(122.271, 238.545);
    vp.pokreniAlgoritam();
    auto p = vp.polygon();
    bool cond = p.containsPoint(QPointF(80, 256), Qt::OddEvenFill);

    EXPECT_TRUE(cond);
}

TEST(visibility_polygon, testVertexInsidePolygon)
{
    VisibilityPolygon vp(nullptr, 0, false, inputFile1, 20);

    vp.initPositionFixed(122.271, 238.545);
    vp.pokreniAlgoritam();
    auto p = vp.polygon();
    bool cond = p.containsPoint(QPointF(29, 238), Qt::OddEvenFill);

    EXPECT_TRUE(cond);
}

TEST(visibility_polygon, testPointOnEdgeInsidePolygon)
{
    VisibilityPolygon vp(nullptr, 0, false, inputFile1, 20);

    vp.initPositionFixed(122.271, 238.545);
    vp.pokreniAlgoritam();
    auto p = vp.polygon();
    bool cond = p.containsPoint(QPointF(160, 422), Qt::OddEvenFill);

    EXPECT_TRUE(cond);
}

TEST(visibility_polygon, testOutPointOutsidePolygon)
{
    VisibilityPolygon vp(nullptr, 0, false, inputFile1, 20);

    vp.initPositionFixed(122.271, 238.545);
    vp.pokreniAlgoritam();
    auto p = vp.polygon();
    bool cond = p.containsPoint(QPointF(50, 150), Qt::OddEvenFill);

    EXPECT_FALSE(cond);
}

TEST(visibility_polygon, testShadowPointOutsidePolygon)
{
    VisibilityPolygon vp(nullptr, 0, false, inputFile1, 20);

    vp.initPositionFixed(122.271, 238.545);
    vp.pokreniAlgoritam();
    auto p = vp.polygon();
    bool cond = p.containsPoint(QPointF(150, 180), Qt::OddEvenFill);

    EXPECT_FALSE(cond);
}

TEST(visibility_polygon, testVertexOutsidePolygon)
{
    VisibilityPolygon vp(nullptr, 0, false, inputFile1, 20);

    vp.initPositionFixed(122.271, 238.545);
    vp.pokreniAlgoritam();
    auto p = vp.polygon();
    bool cond = p.containsPoint(QPointF(83, 119), Qt::OddEvenFill);

    EXPECT_FALSE(cond);
}

#endif // TST_VISIBILITYPOLYGON_H
